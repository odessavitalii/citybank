"use strict";function init(){var a=$("body"),e=$(window),n=a.find(".header"),o=$(".header__nav .nav__link"),t=$("#btnMenu"),i=$("#btnMenuClose"),s=new WOW({boxClass:"wowHeader",animateClass:"animated",offset:0,mobile:!0,live:!0,callback:function(){},scrollContainer:null}),l=$(".page").length,c=!1,r=1,d=n.outerHeight();function u(e,n){c=!0;var a=r-e,o=r;if(r=e,$(".page").removeClass("active small previous"),$(".page-"+e).addClass("active"),$(".nav-btn").removeClass("active"),$(".nav-page"+e).addClass("active"),1<e){if($(".page-"+(e-1)).addClass("previous"),n){$(".page-"+(e-1)).hide();var t=e;setTimeout(function(){$(".page-"+(t-1)).show()},600)}for(;--e;)$(".page-"+e).addClass("small")}if(1<a)for(var i=e+1;i<o;i++)$(".page-"+i+" .half").css("transition","transform .7s ease-out");v(),p(),h($(".page.active")),setTimeout(function(){c=!1,$(".page .half").attr("style","")},700)}function f(){1<r&&u(--r,!0),v(),p()}function m(){r<l&&u(++r),v(),p(),h($(".page.active"))}function v(){1<r?a.addClass("inner-page"):a.removeClass("inner-page")}function p(){o.each(function(e,n){e+1===r-1?(o.removeClass("active"),$(n).addClass("active")):r-1||o.removeClass("active")})}function h(e){if(!e.hasClass("page-animated")){var n=new WOW({boxClass:"wow",animateClass:"animated",offset:0,mobile:!0,live:!0,callback:function(){},scrollContainer:null});e.find(".wow").addClass("wow-removed").removeClass("wow"),e.find(".wow-removed").removeClass("wow-removed").addClass("wow"),e.addClass("page-animated"),n.init()}}if(a.hasClass("main-page")&&767<e.width()&&($(document).on("mousewheel DOMMouseScroll",function(e){c||(0<e.originalEvent.wheelDelta||e.originalEvent.detail<0?f:m)()}),$(document).on("click",".scroll-btn",function(){c||($(this).hasClass("up")?f:m)()}),$(document).on("keydown",function(e){c||(38===e.which?f():40===e.which&&m())}),$(document).on("click",".nav-btn:not(.active)",function(){c||u(+$(this).attr("data-target"))}),$(document).on("click","#scrollTo",function(e){e.preventDefault(),c||u(2)}),$(document).on("click",".header__nav .nav__link",function(e){e.preventDefault();var n=$(this).index();c||u(n+2)}),s.init(),h($(".page.active"))),a.hasClass("main-page")&&e.width()<=767&&($(document).on("click",".menu-drop .nav__link",function(e){e.preventDefault();var n=$(this).attr("href");a.removeClass("is-menu-open"),$("html,body").animate({scrollTop:$(n).offset().top},"slow")}),$("[data-mob-scroll]").on("click",function(e){e.preventDefault();var n=$(this).data("mob-scroll");$("html, body").animate({scrollTop:$("#"+n).offset().top},"slow")}),e.on("scroll",function(e){$(this).scrollTop()?a.addClass("inner-page"):a.removeClass("inner-page")})),a.hasClass("page-inner")&&a.css("padding-top",d),e.width()<=767){var w=new WOW({boxClass:"wow",animateClass:"animated",offset:0,mobile:!0,live:!0,callback:function(){},scrollContainer:null});s.init(),w.init(),t.on("click",function(){a.addClass("is-menu-open")}),i.on("click",function(){a.removeClass("is-menu-open")})}}!function(){var e=window.Element.prototype;"function"!=typeof e.matches&&(e.matches=e.msMatchesSelector||e.mozMatchesSelector||e.webkitMatchesSelector||function(e){for(var n=(this.document||this.ownerDocument).querySelectorAll(e),a=0;n[a]&&n[a]!==this;)++a;return Boolean(n[a])}),"function"!=typeof e.closest&&(e.closest=function(e){for(var n=this;n&&1===n.nodeType;){if(n.matches(e))return n;n=n.parentNode}return null})}(),$(window).on("load",function(){setTimeout(function(){$("body").removeClass("is-preload")},1e3)}),$("document").ready(function(){init()}),$(window).resize(function(){init()});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiXSwibmFtZXMiOlsiJCIsImluaXQiLCIkYm9keSIsIiR3aW5kb3ciLCJ3aW5kb3ciLCIkaGVhZGVyIiwiZmluZCIsIiRuYXZMaW5rIiwiJGJ0bk1lbnUiLCIkYnRuTWVudUNsb3NlIiwid293SGVhZGVyIiwibGl2ZSIsImNhbGxiYWNrIiwic2Nyb2xsQ29udGFpbmVyIiwicGFnZXMiLCJsZW5ndGgiLCJzY3JvbGxpbmciLCJjdXJQYWdlIiwiYm94IiwicGFnaW5hdGlvbiIsInBhZ2UiLCJtb3ZpbmdVcCIsImRpZmYiLCJvbGRQYWdlIiwicmVtb3ZlQ2xhc3MiLCJhZGRDbGFzcyIsImhpZGUiLCJoYWNrUGFnZSIsInNldFRpbWVvdXQiLCJzaG93IiwiaiIsImNzcyIsImNoZWNrSGVhZGVyTmF2QWN0aXZlIiwiY2hlY2tXb3ciLCJjaGVja0lubmVyUGFnZSIsImF0dHIiLCJuYXZpZ2F0ZVVwIiwibmF2aWdhdGVEb3duIiwiZWFjaCIsImluZGV4IiwiaXRlbSIsInBhZ2VBY3RpdmUiLCJoYXNDbGFzcyIsIndvdyIsIldPVyIsIm9mZnNldCIsIm1vYmlsZSIsImFuaW1hdGVDbGFzcyIsIm9uIiwiZSIsIm9yaWdpbmFsRXZlbnQiLCJ3aGVlbERlbHRhIiwiZGV0YWlsIiwiZG9jdW1lbnQiLCJ0aGlzIiwid2hpY2giLCJwcmV2ZW50RGVmYXVsdCIsIm5leHRQYWdlIiwic2Nyb2xsVG9wIiwidG9wIiwiYW5pbWF0ZSIsImlkIiwiZGF0YSIsIiRoZWFkZXJIZWlnaHQiLCJ3aWR0aCIsImJveENsYXNzIiwidCIsIkVsZW1lbnQiLCJwcm90b3R5cGUiLCJtYXRjaGVzIiwibXNNYXRjaGVzU2VsZWN0b3IiLCJtb3pNYXRjaGVzU2VsZWN0b3IiLCJ3ZWJraXRNYXRjaGVzU2VsZWN0b3IiLCJvd25lckRvY3VtZW50IiwicXVlcnlTZWxlY3RvckFsbCIsIm8iLCJCb29sZWFuIiwibm9kZVR5cGUiLCJwYXJlbnROb2RlIiwicmVhZHkiLCJyZXNpemUiXSwibWFwcGluZ3MiOiJhQXVCQUEsU0FBQUMsT0FDSUEsSUFBSUMsRUFBQUYsRUFBQSxRQURSRyxFQUFBSCxFQUFBSSxRQU9RQyxFQUFVSCxFQUFNSSxLQUFLLFdBSDdCQyxFQUFnQlAsRUFBQSwyQkFDTkUsRUFBVUYsRUFBQSxZQUNaRyxFQUFXSCxFQURmLGlCQUVJSyxFQUFVSCxJQUFNSSxJQUFLLENBQ3JCQyxTQUFhLFlBQ2JDLGFBQWEsV0FDYkMsT0FBYSxFQUNiQyxRQUFZLEVBQ0FDLE1BQUUsRUFBa0JDLFNBQUEsYUFFUkMsZ0JBQUEsT0FDQUMsRUFBQWQsRUFBQSxTQUFBZSxPQUNoQkMsR0FMWSxFQUtFQyxFQUFBLEVBQ2xCTCxFQUFVUCxFQUFVYSxjQUduQixTQVRlQyxFQUFBQyxFQUFBQyxHQVVoQlIsR0FBZSxFQWhCdkIsSUFBQVMsRUFBQUwsRUFBQUcsRUFrQlNHLEVBQUtOLEVBV1ZBLEdBVEFBLEVBRkpHLEVBYUlwQixFQUFFLFNBQVN3QixZQUFZLHlCQVIzQnhCLEVBQUEsU0FBQW9CLEdBQUFLLFNBQUEsVUFDSVQsRUFBQUEsWUFBQVEsWUFBQSxVQUVJRixFQUFBQSxZQUFjRixHQUFsQkssU0FBQSxVQUdBLEVBQU9MLEVBQVAsQ0FJRSxHQUZBcEIsRUFBRixVQUFBb0IsRUFBdUIsSUFBQUssU0FBQSxZQUV0QkosRUFBYUcsQ0FDWnhCLEVBQUEsVUFBb0J5QixFQUFTLElBQUFDLE9BRS9CLElBQWNDLEVBQUFQLEVBUU5RLFdBQVcsV0FMRDVCLEVBQUEsVUFBQTJCLEVBQUEsSUFBQUUsUUFDUixLQUtJLE9BQUFULEdBRE5wQixFQUFBLFNBQUFvQixHQUFBSyxTQUFBLFNBTUF6QixHQUFFLEVBQUFzQixFQUNMLElBQUEsSUFBQVEsRUFBQVYsRUFBQSxFQUFBVSxFQUFBUCxFQUFBTyxJQUNKOUIsRUFBQSxTQUFBOEIsRUFBQSxVQUFBQyxJQUFBLGFBQUEsMEJBSU8vQixJQUNIZ0MsSUFDSkMsRUFBQWpDLEVBQUEsaUJBRURrQyxXQUFjLFdBQ2RGLEdBQW9CLEVBR3BCSixFQUFVLGVBQU9PLEtBQUEsUUFBQSxLQUdYLEtBS1YsU0FBU0MsSUFBVCxFQUFTQSxHQUVEbkIsSUFEQUEsR0FDTyxHQUVWaUIsSUFFREYsSUFDSCxTQUFBSyxJQUdPcEIsRUFBVUgsR0FBVkcsSUFEQ29CLEdBR0RsQixJQUNIYSxJQUdEQyxFQUFTakMsRUFBRSxpQkFBWGlDLFNBQVNqQyxJQUNaLEVBQUFpQixFQUlPZixFQUFNdUIsU0FBUyxjQURmUixFQUFVTyxZQUFHLGNBSWhCLFNBQUFRLElBQ0p6QixFQUFBK0IsS0FBQSxTQUFBQyxFQUFBQyxHQUlXRCxFQUFRLElBQU10QixFQUFVLEdBRjNCZSxFQUFBQSxZQUF1QixVQUNuQk0sRUFBS0UsR0FBQWYsU0FBQSxXQUNRUixFQUFsQixHQUNZVixFQUFDaUIsWUFBVCxZQUtQLFNBUERTLEVBQUFRLEdBUUgsSUFBQUEsRUFBQUMsU0FBQSxpQkFBQSxDQUlPLElBQU1DLEVBQU0sSUFBSUMsSUFBSSxDQUZuQlgsU0FBU1EsTUFDVEEsYUFBb0IsV0FDVEksT0FBUSxFQUNSQyxRQURRLEVBQ01uQyxNQUFBLEVBQ3RCb0MsU0FBYyxhQUVObEMsZ0JBSlEsT0FLRTRCLEVBQUFuQyxLQUFBLFFBQUFtQixTQUFBLGVBQUFELFlBQUEsT0FDbEJaLEVBQVVOLEtBQUEsZ0JBQ05rQixZQUFBLGVBQUFDLFNBQUEsT0FDQWdCLEVBQUFoQixTQUFBLGlCQUVKWixFQUFBQSxRQXFISCxHQWpIRDRCLEVBQUFBLFNBQWdCLGNBQTRCLElBQVpqQixFQUFBQSxVQUdoQ21CLEVBQUkxQyxVQUFKK0MsR0FBQSw0QkFBQSxTQUFBQyxHQUNIakMsSUFDSixFQUFBaUMsRUFBQUMsY0FBQUMsWUFBQUYsRUFBQUMsY0FBQUUsT0FBQSxFQUFBaEIsRUFBQUMsT0FLT3JDLEVBQUlxRCxVQUFKTCxHQUFnQixRQUFBLGNBQUEsV0FDVkUsSUFJVmxELEVBQUFzRCxNQUFBWixTQUFBLE1BQUFOLEVBQUFDLE9BSU1yQyxFQUFBcUQsVUFBTVgsR0FBUyxVQUFRTixTQUFBQSxHQUc3QnBCLElBQ0EsS0FBWWdDLEVBQVpPLE1BQ1F2QyxJQUltQixLQUFaaUMsRUFBRU0sT0FGUEEsT0FPVnZELEVBQUFxRCxVQUFBTCxHQUFBLFFBQUEsd0JBQUEsV0FFUWhDLEdBQUFBLEdBQVdoQixFQUFBc0QsTUFBQW5CLEtBQUEsa0JBTWpCa0IsRUFBQUEsVUFBYUwsR0FBQSxRQUFTLFlBQWEsU0FBYUMsR0FDNUNPLEVBQUFBLGlCQUdReEMsR0FHZEcsRUFBQSxLQUlJbkIsRUFBSXVDLFVBQVVTLEdBQUEsUUFBRiwwQkFBWixTQUFBQyxHQUNJUSxFQUFBQSxpQkFJSnRDLElBQVVvQixFQUFDa0IsRUFBWEgsTUFBQWYsUUFJT3ZCLEdBSlBHLEVBUkpvQixFQUFBLEtBaUJNaUIsRUFBQUEsT0FFRnZCLEVBQVdqQyxFQUFBLGtCQUtIMEQsRUFBQUEsU0FBWSxjQUFjQyxFQUFBQSxTQUFBQSxNQURsQzNELEVBR0lxRCxVQUhKTCxHQUFBLFFBQUEsd0JBQUEsU0FBQUMsR0FQSkEsRUFBQU8saUJBY01BLElBQUFBLEVBQUFBLEVBQUZGLE1BQUFuQixLQUFBLFFBSUVqQyxFQUFBc0IsWUFBc0IsZ0JBRXJCeEIsRUFBQSxhQUZINEQsUUFBQSxDQUxKRixVQUFBMUQsRUFBQTZELEdBQUFoQixTQUFBYyxLQVdRRCxVQUdBeEQsRUFBQUEscUJBQWU4QyxHQUFBLFFBQWYsU0FBQUMsR0FESkEsRUFFT08saUJBRU4sSUFBQUssRUFBQTdELEVBQUFzRCxNQUFBUSxLQUFBLGNBRVI5RCxFQUFBLGNBQUE0RCxRQUFBLENBYldGLFVBQVcxRCxFQUFFLElBQU02RCxHQUFJaEIsU0FBU2MsS0FlbENqQixVQVhOdkMsRUFBUTZDLEdBQUcsU0FBVSxTQUFVQyxHQWVuQ2pELEVBQTRCc0QsTUFBQUksWUFFRXhELEVBQUF1QixTQUFBLGNBQ0l2QixFQUFBc0IsWUFBQSxpQkFHMUJiLEVBQU0rQixTQUxVLGVBS0V4QyxFQUFBNkIsSUFBQSxjQUFBZ0MsR0FMRjVELEVBQUE2RCxTQUFBLElBQUEsQ0FVaEJuRCxJQUFBQSxFQUFBQSxJQUFpQitCLElBQUssQ0FUdEJxQixTQUFVLE1BRGRsQixhQUFBLFdBYUFyQyxPQUFBLEVBQ0FvQyxRQUFBLEVBRVFuQyxNQUFJLEVBQ0ZjLFNBQVMsYUFHbkJaLGdCQUFBLE9BUEFILEVBQVVULE9BQ1YwQyxFQUFJMUMsT0FFSk8sRUFBU3dDLEdBQUcsUUFBUyxXQUNqQjlDLEVBQU11QixTQUFTLGtCQUduQmhCLEVBQWN1QyxHQUFHLFFBQVMsV0FDdEI5QyxFQUFNc0IsWUFBWSxvQkF2UjdCLFdBQ0EsSUFBSTBDLEVBTUY5RCxPQU5RK0QsUUFBUUMsVUFEbEIsbUJBQVlGLEVBQUFHLFVBQUFILEVBQUFHLFFBQUFILEVBQUFJLG1CQUFBSixFQUFBSyxvQkFBQUwsRUFBQU0sdUJBQUEsU0FBQXZCLEdBQ1osSUFBSyxJQUFJaUIsR0FBQ0MsS0FBUUMsVUFBbEJkLEtBQUFtQixlQUFBQyxpQkFBQXpCLEdBQUEwQixFQUFBLEVBQUFULEVBQUFTLElBQUFULEVBQUFTLEtBQUFyQixRQUFBcUIsRUFDQSxPQUFBQyxRQUFjVixFQUFBUyxNQUNiLG1CQUFtQnRCLEVBQUFBLFVBQWlCb0IsRUFBQUEsUUFBQUEsU0FBZUMsR0FBb0QsSUFBQSxJQUFBUixFQUFBWixLQUFBWSxHQUFBLElBQUFBLEVBQUFXLFVBQUEsQ0FBdkcsR0FBQVgsRUFBQUcsUUFBQXBCLEdBQUEsT0FBQWlCLEVBS0NBLEVBQUlBLEVBQUVZLFdBSFAsT0FBRyxPQUxKLEdBUUVaLEVBQUFBLFFBQUlBLEdBQUVZLE9BQUFBLFdBQ05sRCxXQUFBLFdBTUs1QixFQUFFLFFBQVF3QixZQUFZLGVBTDVCLE9BR0Z4QixFQUFFSSxZQUFXMkUsTUFBYixXQUNJbkQsU0FHSDVCLEVBSkRJLFFBQUE0RSxPQUFBLFdBTUUvRSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIWZ1bmN0aW9uKGUpIHtcblx0dmFyIHQgPSBlLkVsZW1lbnQucHJvdG90eXBlO1xuXHRcImZ1bmN0aW9uXCIgIT0gdHlwZW9mIHQubWF0Y2hlcyAmJiAodC5tYXRjaGVzID0gdC5tc01hdGNoZXNTZWxlY3RvciB8fCB0Lm1vek1hdGNoZXNTZWxlY3RvciB8fCB0LndlYmtpdE1hdGNoZXNTZWxlY3RvciB8fCBmdW5jdGlvbihlKSB7XG5cdFx0Zm9yICh2YXIgdCA9ICh0aGlzLmRvY3VtZW50IHx8IHRoaXMub3duZXJEb2N1bWVudCkucXVlcnlTZWxlY3RvckFsbChlKSwgbyA9IDA7IHRbb10gJiYgdFtvXSAhPT0gdGhpczspICsrbztcblx0XHRyZXR1cm4gQm9vbGVhbih0W29dKVxuXHR9KSwgXCJmdW5jdGlvblwiICE9IHR5cGVvZiB0LmNsb3Nlc3QgJiYgKHQuY2xvc2VzdCA9IGZ1bmN0aW9uKGUpIHtcblx0XHRmb3IgKHZhciB0ID0gdGhpczsgdCAmJiAxID09PSB0Lm5vZGVUeXBlOykge1xuXHRcdFx0aWYgKHQubWF0Y2hlcyhlKSkgcmV0dXJuIHQ7XG5cdFx0XHR0ID0gdC5wYXJlbnROb2RlXG5cdFx0fVxuXHRcdHJldHVybiBudWxsXG5cdH0pXG59KHdpbmRvdyk7XG4kKHdpbmRvdykub24oJ2xvYWQnLCBmdW5jdGlvbiAoKSB7XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICQoJ2JvZHknKS5yZW1vdmVDbGFzcygnaXMtcHJlbG9hZCcpO1xuICAgIH0sIDEwMDApO1xufSk7XG5cbiQoJ2RvY3VtZW50JykucmVhZHkoZnVuY3Rpb24gKCkge1xuICAgIGluaXQoKTtcbn0pO1xuXG4kKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uICgpIHtcbiAgICBpbml0KCk7XG59KTtcblxuZnVuY3Rpb24gaW5pdCgpIHtcbiAgICBjb25zdCAkYm9keSA9ICQoJ2JvZHknKSxcbiAgICAgICAgJHdpbmRvdyA9ICQod2luZG93KSxcbiAgICAgICAgJGhlYWRlciA9ICRib2R5LmZpbmQoJy5oZWFkZXInKSxcbiAgICAgICAgJG5hdkxpbmsgPSAkKCcuaGVhZGVyX19uYXYgLm5hdl9fbGluaycpLFxuICAgICAgICAkYnRuTWVudSA9ICQoJyNidG5NZW51JyksXG4gICAgICAgICRidG5NZW51Q2xvc2UgPSAkKCcjYnRuTWVudUNsb3NlJyksXG4gICAgICAgIHdvd0hlYWRlciA9IG5ldyBXT1coe1xuICAgICAgICAgICAgYm94Q2xhc3M6ICd3b3dIZWFkZXInLCAgICAgIC8vIGFuaW1hdGVkIGVsZW1lbnQgY3NzIGNsYXNzIChkZWZhdWx0IGlzIHdvdylcbiAgICAgICAgICAgIGFuaW1hdGVDbGFzczogJ2FuaW1hdGVkJywgLy8gYW5pbWF0aW9uIGNzcyBjbGFzcyAoZGVmYXVsdCBpcyBhbmltYXRlZClcbiAgICAgICAgICAgIG9mZnNldDogMCwgICAgICAgICAgLy8gZGlzdGFuY2UgdG8gdGhlIGVsZW1lbnQgd2hlbiB0cmlnZ2VyaW5nIHRoZSBhbmltYXRpb24gKGRlZmF1bHQgaXMgMClcbiAgICAgICAgICAgIG1vYmlsZTogdHJ1ZSwgICAgICAgLy8gdHJpZ2dlciBhbmltYXRpb25zIG9uIG1vYmlsZSBkZXZpY2VzIChkZWZhdWx0IGlzIHRydWUpXG4gICAgICAgICAgICBsaXZlOiB0cnVlLCAgICAgICAvLyBhY3Qgb24gYXN5bmNocm9ub3VzbHkgbG9hZGVkIGNvbnRlbnQgKGRlZmF1bHQgaXMgdHJ1ZSlcbiAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbiAoYm94KSB7XG4gICAgICAgICAgICAgICAgLy8gdGhlIGNhbGxiYWNrIGlzIGZpcmVkIGV2ZXJ5IHRpbWUgYW4gYW5pbWF0aW9uIGlzIHN0YXJ0ZWRcbiAgICAgICAgICAgICAgICAvLyB0aGUgYXJndW1lbnQgdGhhdCBpcyBwYXNzZWQgaW4gaXMgdGhlIERPTSBub2RlIGJlaW5nIGFuaW1hdGVkXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgc2Nyb2xsQ29udGFpbmVyOiBudWxsIC8vIG9wdGlvbmFsIHNjcm9sbCBjb250YWluZXIgc2VsZWN0b3IsIG90aGVyd2lzZSB1c2Ugd2luZG93XG4gICAgICAgIH0pO1xuICAgIGxldCBwYWdlcyA9ICQoXCIucGFnZVwiKS5sZW5ndGgsXG4gICAgICAgIHNjcm9sbGluZyA9IGZhbHNlLFxuICAgICAgICBjdXJQYWdlID0gMSxcbiAgICAgICAgJGhlYWRlckhlaWdodCA9ICRoZWFkZXIub3V0ZXJIZWlnaHQoKTtcblxuICAgIGZ1bmN0aW9uIHBhZ2luYXRpb24ocGFnZSwgbW92aW5nVXApIHtcbiAgICAgICAgc2Nyb2xsaW5nID0gdHJ1ZTtcblxuICAgICAgICBsZXQgZGlmZiA9IGN1clBhZ2UgLSBwYWdlLFxuICAgICAgICAgICAgb2xkUGFnZSA9IGN1clBhZ2U7XG5cbiAgICAgICAgY3VyUGFnZSA9IHBhZ2U7XG5cbiAgICAgICAgJChcIi5wYWdlXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlIHNtYWxsIHByZXZpb3VzXCIpO1xuICAgICAgICAkKFwiLnBhZ2UtXCIgKyBwYWdlKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICAgJChcIi5uYXYtYnRuXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICAkKFwiLm5hdi1wYWdlXCIgKyBwYWdlKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcblxuICAgICAgICBpZiAocGFnZSA+IDEpIHtcbiAgICAgICAgICAgICQoXCIucGFnZS1cIiArIChwYWdlIC0gMSkpLmFkZENsYXNzKFwicHJldmlvdXNcIik7XG5cbiAgICAgICAgICAgIGlmIChtb3ZpbmdVcCkge1xuICAgICAgICAgICAgICAgICQoXCIucGFnZS1cIiArIChwYWdlIC0gMSkpLmhpZGUoKTtcblxuICAgICAgICAgICAgICAgIGxldCBoYWNrUGFnZSA9IHBhZ2U7XG5cbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgJChcIi5wYWdlLVwiICsgKGhhY2tQYWdlIC0gMSkpLnNob3coKTtcbiAgICAgICAgICAgICAgICB9LCA2MDApO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB3aGlsZSAoLS1wYWdlKSB7XG4gICAgICAgICAgICAgICAgJChcIi5wYWdlLVwiICsgcGFnZSkuYWRkQ2xhc3MoXCJzbWFsbFwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChkaWZmID4gMSkge1xuICAgICAgICAgICAgZm9yIChsZXQgaiA9IHBhZ2UgKyAxOyBqIDwgb2xkUGFnZTsgaisrKSB7XG4gICAgICAgICAgICAgICAgJChcIi5wYWdlLVwiICsgaiArIFwiIC5oYWxmXCIpLmNzcyhcInRyYW5zaXRpb25cIiwgXCJ0cmFuc2Zvcm0gLjdzIGVhc2Utb3V0XCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgY2hlY2tJbm5lclBhZ2UoKTtcbiAgICAgICAgY2hlY2tIZWFkZXJOYXZBY3RpdmUoKTtcbiAgICAgICAgY2hlY2tXb3coJCgnLnBhZ2UuYWN0aXZlJykpO1xuXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgc2Nyb2xsaW5nID0gZmFsc2U7XG5cbiAgICAgICAgICAgICQoXCIucGFnZSAuaGFsZlwiKS5hdHRyKFwic3R5bGVcIiwgXCJcIik7XG5cbiAgICAgICAgfSwgNzAwKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBuYXZpZ2F0ZVVwKCkge1xuICAgICAgICBpZiAoY3VyUGFnZSA+IDEpIHtcbiAgICAgICAgICAgIGN1clBhZ2UtLTtcbiAgICAgICAgICAgIHBhZ2luYXRpb24oY3VyUGFnZSwgdHJ1ZSk7XG4gICAgICAgIH1cbiAgICAgICAgY2hlY2tJbm5lclBhZ2UoKTtcbiAgICAgICAgY2hlY2tIZWFkZXJOYXZBY3RpdmUoKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBuYXZpZ2F0ZURvd24oKSB7XG4gICAgICAgIGlmIChjdXJQYWdlIDwgcGFnZXMpIHtcbiAgICAgICAgICAgIGN1clBhZ2UrKztcbiAgICAgICAgICAgIHBhZ2luYXRpb24oY3VyUGFnZSk7XG4gICAgICAgIH1cbiAgICAgICAgY2hlY2tJbm5lclBhZ2UoKTtcbiAgICAgICAgY2hlY2tIZWFkZXJOYXZBY3RpdmUoKTtcbiAgICAgICAgY2hlY2tXb3coJCgnLnBhZ2UuYWN0aXZlJykpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNoZWNrSW5uZXJQYWdlKCkge1xuICAgICAgICBpZiAoY3VyUGFnZSA+IDEpIHtcbiAgICAgICAgICAgICRib2R5LmFkZENsYXNzKCdpbm5lci1wYWdlJylcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICRib2R5LnJlbW92ZUNsYXNzKCdpbm5lci1wYWdlJylcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNoZWNrSGVhZGVyTmF2QWN0aXZlKCkge1xuICAgICAgICAkbmF2TGluay5lYWNoKChpbmRleCwgaXRlbSkgPT4ge1xuICAgICAgICAgICAgaWYgKGluZGV4ICsgMSA9PT0gY3VyUGFnZSAtIDEpIHtcbiAgICAgICAgICAgICAgICAkbmF2TGluay5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgJChpdGVtKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKCEoY3VyUGFnZSAtIDEpKSB7XG4gICAgICAgICAgICAgICAgJG5hdkxpbmsucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNoZWNrV293KHBhZ2VBY3RpdmUpIHtcbiAgICAgICAgaWYgKCFwYWdlQWN0aXZlLmhhc0NsYXNzKCdwYWdlLWFuaW1hdGVkJykpIHtcbiAgICAgICAgICAgIGNvbnN0IHdvdyA9IG5ldyBXT1coe1xuICAgICAgICAgICAgICAgIGJveENsYXNzOiAnd293JywgICAgICAvLyBhbmltYXRlZCBlbGVtZW50IGNzcyBjbGFzcyAoZGVmYXVsdCBpcyB3b3cpXG4gICAgICAgICAgICAgICAgYW5pbWF0ZUNsYXNzOiAnYW5pbWF0ZWQnLCAvLyBhbmltYXRpb24gY3NzIGNsYXNzIChkZWZhdWx0IGlzIGFuaW1hdGVkKVxuICAgICAgICAgICAgICAgIG9mZnNldDogMCwgICAgICAgICAgLy8gZGlzdGFuY2UgdG8gdGhlIGVsZW1lbnQgd2hlbiB0cmlnZ2VyaW5nIHRoZSBhbmltYXRpb24gKGRlZmF1bHQgaXMgMClcbiAgICAgICAgICAgICAgICBtb2JpbGU6IHRydWUsICAgICAgIC8vIHRyaWdnZXIgYW5pbWF0aW9ucyBvbiBtb2JpbGUgZGV2aWNlcyAoZGVmYXVsdCBpcyB0cnVlKVxuICAgICAgICAgICAgICAgIGxpdmU6IHRydWUsICAgICAgIC8vIGFjdCBvbiBhc3luY2hyb25vdXNseSBsb2FkZWQgY29udGVudCAoZGVmYXVsdCBpcyB0cnVlKVxuICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbiAoYm94KSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIHRoZSBjYWxsYmFjayBpcyBmaXJlZCBldmVyeSB0aW1lIGFuIGFuaW1hdGlvbiBpcyBzdGFydGVkXG4gICAgICAgICAgICAgICAgICAgIC8vIHRoZSBhcmd1bWVudCB0aGF0IGlzIHBhc3NlZCBpbiBpcyB0aGUgRE9NIG5vZGUgYmVpbmcgYW5pbWF0ZWRcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHNjcm9sbENvbnRhaW5lcjogbnVsbCAvLyBvcHRpb25hbCBzY3JvbGwgY29udGFpbmVyIHNlbGVjdG9yLCBvdGhlcndpc2UgdXNlIHdpbmRvd1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHBhZ2VBY3RpdmUuZmluZCgnLndvdycpLmFkZENsYXNzKCd3b3ctcmVtb3ZlZCcpLnJlbW92ZUNsYXNzKCd3b3cnKTtcbiAgICAgICAgICAgIHBhZ2VBY3RpdmUuZmluZCgnLndvdy1yZW1vdmVkJykucmVtb3ZlQ2xhc3MoJ3dvdy1yZW1vdmVkJykuYWRkQ2xhc3MoJ3dvdycpO1xuICAgICAgICAgICAgcGFnZUFjdGl2ZS5hZGRDbGFzcygncGFnZS1hbmltYXRlZCcpO1xuXG4gICAgICAgICAgICB3b3cuaW5pdCgpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgaWYgKCRib2R5Lmhhc0NsYXNzKCdtYWluLXBhZ2UnKSAmJiAkd2luZG93LndpZHRoKCkgPiA3NjcpIHtcbiAgICAgICAgLy8gbW92ZSB0byBwYWdlIGFmdGVyIG1vdXNld2hlZWxcbiAgICAgICAgJChkb2N1bWVudCkub24oXCJtb3VzZXdoZWVsIERPTU1vdXNlU2Nyb2xsXCIsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICBpZiAoIXNjcm9sbGluZykge1xuICAgICAgICAgICAgICAgIGUub3JpZ2luYWxFdmVudC53aGVlbERlbHRhID4gMCB8fCBlLm9yaWdpbmFsRXZlbnQuZGV0YWlsIDwgMCA/IG5hdmlnYXRlVXAoKSA6IG5hdmlnYXRlRG93bigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAvLyBtb3ZlIHRvIHBhZ2UgYWZ0ZXIgY2xpY2sgbmF2IGJ0blxuICAgICAgICAkKGRvY3VtZW50KS5vbihcImNsaWNrXCIsIFwiLnNjcm9sbC1idG5cIiwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYgKHNjcm9sbGluZykgcmV0dXJuO1xuXG4gICAgICAgICAgICAkKHRoaXMpLmhhc0NsYXNzKFwidXBcIikgPyBuYXZpZ2F0ZVVwKCkgOiBuYXZpZ2F0ZURvd24oKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gbW92ZSB0byBwYWdlIGFmdGVyIGtleWRvd25cbiAgICAgICAgJChkb2N1bWVudCkub24oXCJrZXlkb3duXCIsIGUgPT4ge1xuICAgICAgICAgICAgaWYgKHNjcm9sbGluZykgcmV0dXJuO1xuXG4gICAgICAgICAgICBpZiAoZS53aGljaCA9PT0gMzgpIHtcbiAgICAgICAgICAgICAgICBuYXZpZ2F0ZVVwKCk7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGUud2hpY2ggPT09IDQwKSB7XG4gICAgICAgICAgICAgICAgbmF2aWdhdGVEb3duKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIG1vdmUgdG8gcGFnZSBhZnRlciBwYWdpbmF0aW9uIGRvdFxuICAgICAgICAkKGRvY3VtZW50KS5vbihcImNsaWNrXCIsIFwiLm5hdi1idG46bm90KC5hY3RpdmUpXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmIChzY3JvbGxpbmcpIHJldHVybjtcblxuICAgICAgICAgICAgcGFnaW5hdGlvbigrJCh0aGlzKS5hdHRyKFwiZGF0YS10YXJnZXRcIikpO1xuICAgICAgICB9KTtcblxuICAgICAgICAvLyBtb3ZlIHRvIHBhZ2UgMlxuICAgICAgICAkKGRvY3VtZW50KS5vbihcImNsaWNrXCIsICcjc2Nyb2xsVG8nLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICBpZiAoc2Nyb2xsaW5nKSByZXR1cm47XG4gICAgICAgICAgICBwYWdpbmF0aW9uKDIpO1xuICAgICAgICB9KTtcblxuICAgICAgICAvLyBtb3ZlIHRvIHBhZ2UgYWZ0ZXIgY2xpY2sgbmF2X19saW5rIGluIGhlYWRlclxuICAgICAgICAkKGRvY3VtZW50KS5vbihcImNsaWNrXCIsIFwiLmhlYWRlcl9fbmF2IC5uYXZfX2xpbmtcIiwgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgbGV0IGluZGV4ID0gJCh0aGlzKS5pbmRleCgpO1xuICAgICAgICAgICAgbGV0IG5leHRQYWdlID0gaW5kZXggKyAyO1xuXG4gICAgICAgICAgICBpZiAoc2Nyb2xsaW5nKSByZXR1cm47XG5cbiAgICAgICAgICAgIHBhZ2luYXRpb24obmV4dFBhZ2UpO1xuICAgICAgICB9KVxuXG4gICAgICAgIHdvd0hlYWRlci5pbml0KCk7XG4gICAgICAgIGNoZWNrV293KCQoJy5wYWdlLmFjdGl2ZScpKTtcbiAgICB9XG5cbiAgICBpZiAoJGJvZHkuaGFzQ2xhc3MoJ21haW4tcGFnZScpICYmICR3aW5kb3cud2lkdGgoKSA8PSA3NjcgKSB7XG4gICAgICAgICQoZG9jdW1lbnQpLm9uKFwiY2xpY2tcIiwgXCIubWVudS1kcm9wIC5uYXZfX2xpbmtcIiwgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgICAgbGV0IGlkID0gJCh0aGlzKS5hdHRyKFwiaHJlZlwiKTtcblxuICAgICAgICAgICAgJGJvZHkucmVtb3ZlQ2xhc3MoJ2lzLW1lbnUtb3BlbicpO1xuXG4gICAgICAgICAgICAkKCdodG1sLGJvZHknKS5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgc2Nyb2xsVG9wOiAkKGlkKS5vZmZzZXQoKS50b3BcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICdzbG93Jyk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQoXCJbZGF0YS1tb2Itc2Nyb2xsXVwiKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICBsZXQgaWQgPSAkKHRoaXMpLmRhdGEoXCJtb2Itc2Nyb2xsXCIpO1xuXG4gICAgICAgICAgICAkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgc2Nyb2xsVG9wOiAkKFwiI1wiICsgaWQpLm9mZnNldCgpLnRvcFxuICAgICAgICAgICAgfSwgJ3Nsb3cnKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgJHdpbmRvdy5vbignc2Nyb2xsJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgIGxldCBzY3JvbGxUb3AgPSAkKHRoaXMpLnNjcm9sbFRvcCgpO1xuXG4gICAgICAgICAgICBpZiAoc2Nyb2xsVG9wKSB7XG4gICAgICAgICAgICAgICAgJGJvZHkuYWRkQ2xhc3MoJ2lubmVyLXBhZ2UnKVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkYm9keS5yZW1vdmVDbGFzcygnaW5uZXItcGFnZScpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgaWYgKCRib2R5Lmhhc0NsYXNzKCdwYWdlLWlubmVyJykpIHtcbiAgICAgICAgJGJvZHkuY3NzKCdwYWRkaW5nLXRvcCcsICRoZWFkZXJIZWlnaHQpO1xuICAgIH1cblxuICAgIGlmICgkd2luZG93LndpZHRoKCkgPD0gNzY3KSB7XG4gICAgICAgIGNvbnN0IHdvdyA9IG5ldyBXT1coe1xuICAgICAgICAgICAgYm94Q2xhc3M6ICd3b3cnLCAgICAgIC8vIGFuaW1hdGVkIGVsZW1lbnQgY3NzIGNsYXNzIChkZWZhdWx0IGlzIHdvdylcbiAgICAgICAgICAgIGFuaW1hdGVDbGFzczogJ2FuaW1hdGVkJywgLy8gYW5pbWF0aW9uIGNzcyBjbGFzcyAoZGVmYXVsdCBpcyBhbmltYXRlZClcbiAgICAgICAgICAgIG9mZnNldDogMCwgICAgICAgICAgLy8gZGlzdGFuY2UgdG8gdGhlIGVsZW1lbnQgd2hlbiB0cmlnZ2VyaW5nIHRoZSBhbmltYXRpb24gKGRlZmF1bHQgaXMgMClcbiAgICAgICAgICAgIG1vYmlsZTogdHJ1ZSwgICAgICAgLy8gdHJpZ2dlciBhbmltYXRpb25zIG9uIG1vYmlsZSBkZXZpY2VzIChkZWZhdWx0IGlzIHRydWUpXG4gICAgICAgICAgICBsaXZlOiB0cnVlLCAgICAgICAvLyBhY3Qgb24gYXN5bmNocm9ub3VzbHkgbG9hZGVkIGNvbnRlbnQgKGRlZmF1bHQgaXMgdHJ1ZSlcbiAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbiAoYm94KSB7XG4gICAgICAgICAgICAgICAgLy8gdGhlIGNhbGxiYWNrIGlzIGZpcmVkIGV2ZXJ5IHRpbWUgYW4gYW5pbWF0aW9uIGlzIHN0YXJ0ZWRcbiAgICAgICAgICAgICAgICAvLyB0aGUgYXJndW1lbnQgdGhhdCBpcyBwYXNzZWQgaW4gaXMgdGhlIERPTSBub2RlIGJlaW5nIGFuaW1hdGVkXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgc2Nyb2xsQ29udGFpbmVyOiBudWxsIC8vIG9wdGlvbmFsIHNjcm9sbCBjb250YWluZXIgc2VsZWN0b3IsIG90aGVyd2lzZSB1c2Ugd2luZG93XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHdvd0hlYWRlci5pbml0KCk7XG4gICAgICAgIHdvdy5pbml0KCk7XG5cbiAgICAgICAgJGJ0bk1lbnUub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJGJvZHkuYWRkQ2xhc3MoJ2lzLW1lbnUtb3BlbicpO1xuICAgICAgICB9KTtcblxuICAgICAgICAkYnRuTWVudUNsb3NlLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICRib2R5LnJlbW92ZUNsYXNzKCdpcy1tZW51LW9wZW4nKTtcbiAgICAgICAgfSlcbiAgICB9XG59Il19
